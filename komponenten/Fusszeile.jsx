
export default function Fusszeile() {
    return (
      <div className="d-flex justify-content-center fixed-bottom text-secondary bg-light pt-2">
        <h6>🥡 Liefermax - 📞 05439 412 99 60 - ⏱️ Mo-So: 10:00 - 22:00 Uhr </h6>
      </div>
    )
}
